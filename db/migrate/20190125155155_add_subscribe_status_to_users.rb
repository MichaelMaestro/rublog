class AddSubscribeStatusToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :subscribe_status, :integer, default: 1
    add_column :users, :integer, :string
  end
end
