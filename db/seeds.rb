# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
User.create(username: 'Father', email: 'misterjoker161@gmail.com', password: '123456', is_admin: true, avatar: 'https://sabotagetimes.com/.image/t_share/MTI5NDgzNzA0OTg4MDY5ODU4/godfather.png')