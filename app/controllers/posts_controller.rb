class PostsController < ApplicationController

	before_action :set_post, only: [:show, :edit, :update, :destroy]

	def index
		@posts = Post.all
	end

	def show
	end

	def new
		@post = Post.new
	end

	def create  
		@post = Post.new(post_params)
		@post.user_id = current_user.id
		if @post.save
			redirect_to @post
		else
			render :new
		end
	end

	def edit 
	end

	def update
		if @post.update_attributes(post_params)
			redirect_to @post, success: 'Статья успешно обновлена' 
		else
			render :edit, danger: 'Статья не обновлена'
		end 
	end

	def destroy
		@post.destroy
		redirect_to posts_path
	end

	def digest 
		if params[:subscribe_status].blank? || ![1,2,3].include?(params[:subscribe_status].to_i)
  			response = { "error" => "нет такого статуса" }
  			render json: response, status: :bad_request
  			return
		end
		@subscribe_status = params[:subscribe_status]
		@users = User.where(is_admin: false, subscribe: true, subscribe_status: @subscribe_status)
		@posts = Post.includes(:user).where(users: {is_admin: true})
		
		if @subscribe_status = 1
			@date = Time.now - 1.days
			@posts = @posts.where(created_at: @date.beginning_of_day..@date.end_of_day) 
		elsif @subscribe_status = 2  
			@date = Time.now - 1.week
			@posts = @posts.where(created_at: @date.beginning_of_day..@date.end_of_day) 
		else
			@date = Time.now - 1.month
			@posts = @posts.where(created_at: @date.beginning_of_day..@date.end_of_day) 
		end

		@users.each do |user|
			ap user
			PostMailer.new_post_email(user, @posts).deliver_now 	
		end	

		return render json: true	
	end

private
	def set_post
		@post = Post.find(params[:id])
	end

	def post_params
		params.require(:post).permit(:title, :summary, :body, :background, :all_tags) 
	end
end