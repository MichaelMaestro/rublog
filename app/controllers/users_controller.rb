class UsersController < ApplicationController

	before_action :set_user, only: [:index, :edit, :update, :destroy]

	def index
	end

	def edit
	end

	def update
		if @user.update_attributes(user_params)
			redirect_to root_path, success: 'Данные пользователя обновлены' 
		else
			render :edit, danger: 'Новые данные не сохранены'
		end 
	end

	def subscribe
		@user = current_user
		@user.update_attribute(:subscribe, true)
		@user.save
		redirect_to root_path
	end

	def unsubscribe
		@user = current_user
		@user.update_attribute(:subscribe, false)
		@user.save
		redirect_to root_path
	end


	private
		def set_user
			@user = current_user
		end

		def user_params
			params.require(:user).permit(:username, :email, :avatar, :subscribe_status) 
		end

end

