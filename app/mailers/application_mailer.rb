class ApplicationMailer < ActionMailer::Base
  default from: 'creator@bot.com'
  layout 'mailer'
end
