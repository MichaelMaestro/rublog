class PostMailer < ApplicationMailer
  default from: 'creator@bot.com'

  def new_post_email user, posts
  	@user = user
  	@posts = posts
    mail(to: user.email, subject: 'Hey, take a apple pie from Father!')
  end
  
end
