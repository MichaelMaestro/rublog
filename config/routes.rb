Rails.application.routes.draw do
	root 'posts#index'
	devise_for :users 
	resources :posts  
	resources :tags, only: [:show]
	resources :users	
	get '/subscribe', to: 'users#subscribe' 
	get '/unsubscribe', to: 'users#unsubscribe'
	get '/digest', to: 'posts#digest' 
end
